﻿public enum CodeEvent
{
    StartGame,
    PauseGame,
    EndGame,
    LoseGame,
    WinGame,
    InputHorizontal,
    InputVertical,
    InputFire,
    RequestScreenSize,
    AnswerScreenSize,
    DeathPlayer,
    DestroyAsteroid
}
