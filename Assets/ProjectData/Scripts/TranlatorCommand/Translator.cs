﻿public delegate void ActionEvent(CodeEvent codeEvent, object data);
public delegate void ActionSignal(CodeEvent codeEvent);

public class Translator
{
    private static event ActionEvent _onActionEvent;
    private static event ActionSignal _onActionSignal;

    public static void RemoveAll()
    {
        _onActionEvent = null;
        _onActionSignal = null;
    }

    public static void AddListener(ActionEvent listener)
    {
        if (listener != null)
            _onActionEvent += listener;
    }

    public static void RemoveListener(ActionEvent listener)
    {
        if (listener != null)
            _onActionEvent -= listener;
    }

    public static void SendEvent(CodeEvent codeEvent, object data)
    {
        _onActionEvent?.Invoke(codeEvent, data);
    }

    public static void AddListener(ActionSignal listener)
    {
        if (listener != null)
            _onActionSignal += listener;
    }

    public static void RemoveListener(ActionSignal listener)
    {
        if (listener != null)
            _onActionSignal -= listener;
    }

    public static void SendSignal(CodeEvent codeEvent)
    {
        _onActionSignal?.Invoke(codeEvent);
    }
}
