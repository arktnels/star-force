﻿public interface ICustomSignal
{
    void OnCustomSignal(CodeEvent codeEvent);
}
