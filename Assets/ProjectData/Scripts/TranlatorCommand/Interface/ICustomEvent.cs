﻿public interface ICustomEvent
{
    void OnCustomEvent(CodeEvent codeEvent, object data);
}
