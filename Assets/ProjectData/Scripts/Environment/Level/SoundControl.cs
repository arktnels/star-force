﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Sounds
{
    Button,
    Win,
    Lose,
    Shoot,
    Dead,
    Crash
}

public class SoundControl : MonoBehaviour
{
    [SerializeField] private AudioSource _sourceLoop;
    [SerializeField] private AudioSource _source;
    [SerializeField] private AudioClip _clickButton;
    [SerializeField] private AudioClip _win;
    [SerializeField] private AudioClip _lose;
    [SerializeField] private AudioClip _shoot;
    [SerializeField] private AudioClip _dead;
    [SerializeField] private AudioClip _crashAsteroid;

    public static SoundControl instance;

    private void Start()
    {
        instance = this;
    }

    public void PlaySound(Sounds sound)
    {
        AudioClip clip = _clickButton;
        switch (sound)
        {
            case Sounds.Win:
                _sourceLoop.Stop();
                clip = _win;
                break;
            case Sounds.Lose:
                _sourceLoop.Stop();
                clip = _lose;
                break;
            case Sounds.Shoot:
                clip = _shoot;
                break;
            case Sounds.Dead:
                clip = _dead;
                break;
            case Sounds.Crash:
                clip = _crashAsteroid;
                break;
        }
        _source.PlayOneShot(clip);
    }
}
