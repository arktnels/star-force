﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuControl : MonoBehaviour, ICustomSignal
{
    [SerializeField] private GameObject _playCanvas;
    [SerializeField] private GameObject _winCanvas;
    [SerializeField] private GameObject _loseCanvas;

    void Start()
    {
        Translator.AddListener(OnCustomSignal);
    }

    public void ButtonRestart()
    {
        SoundControl.instance.PlaySound(Sounds.Button);
        SceneManager.LoadScene("Level");
    }

    public void ButtonBack()
    {
        SoundControl.instance.PlaySound(Sounds.Button);
        SceneManager.LoadScene("Menu");
    }

    public void OnCustomSignal(CodeEvent codeEvent)
    {
        switch (codeEvent)
        {
            case CodeEvent.LoseGame:
                SoundControl.instance.PlaySound(Sounds.Lose);
                _playCanvas.SetActive(false);
                _loseCanvas.SetActive(true);
                break;
            case CodeEvent.WinGame:
                SoundControl.instance.PlaySound(Sounds.Win);
                _playCanvas.SetActive(false);
                _winCanvas.SetActive(true);
                break;
        }
    }
}
