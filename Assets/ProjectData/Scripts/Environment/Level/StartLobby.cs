﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartLobby : MonoBehaviour
{
    [SerializeField] private Levels _levels;
    [SerializeField] private ButtonInfo[] _buttonsInfo;
    [SerializeField] private AudioSource _source;
    [SerializeField] private AudioClip _clickButton;

    void Start()
    {
        _levels.Load();
        SetButton();
    }

    private void SetButton()
    {
        for (int i = 0; i < _buttonsInfo.Length; i++)
        {
            StateLevel state = _levels.GetStateLevel(i);
            ButtonInfo button = _buttonsInfo[i];
            switch (state)
            {
                case StateLevel.Ready:
                    button.button.interactable = false;
                    button.readyImage.SetActive(true);
                    break;
                case StateLevel.Open:
                    button.button.interactable = true;
                    button.readyImage.SetActive(false);
                    break;
                case StateLevel.Close:
                default:
                    button.button.interactable = false;
                    button.readyImage.SetActive(false);
                    break;
            }
        }
    }

    public void ButtonLevel()
    {
        _source.PlayOneShot(_clickButton);
        SceneManager.LoadScene("Level");
    }

    public void RestartSave()
    {
        _source.PlayOneShot(_clickButton);
        _levels.Clear();
        SetButton();
    }
}
