﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "StarForce/Levels", fileName = "Levels", order = 51)]
public class Levels : ScriptableObject
{
    public LevelInfo levelInfo;

    public int GetCurrentLevel()
    {
        for (int i = 0; i < levelInfo.stateLevels.Count; i++)
            if (levelInfo.stateLevels[i] == StateLevel.Open)
                return i;

        return -1;
    }

    public StateLevel GetStateLevel(int level)
    {
        if (level < levelInfo.stateLevels.Count)
            return levelInfo.stateLevels[level];

        return StateLevel.Close;
    }

    public void NextLevel()
    {
        int currentLevel = GetCurrentLevel();
        levelInfo.stateLevels[currentLevel] = StateLevel.Ready;

        if (currentLevel + 1 < levelInfo.stateLevels.Count)
            levelInfo.stateLevels[currentLevel + 1] = StateLevel.Open;

        Save();
    }

    public void Save()
    {
        string s = JsonUtility.ToJson(levelInfo);

        PlayerPrefs.SetString("GameProgress", s);
    }

    public void Load()
    {
        if (PlayerPrefs.HasKey("GameProgress"))
        {
            string s = PlayerPrefs.GetString("GameProgress");
            levelInfo = JsonUtility.FromJson<LevelInfo>(s);
        }
    }

    public void Clear()
    {
        if (PlayerPrefs.HasKey("GameProgress"))
        {
            PlayerPrefs.DeleteKey("GameProgress");
        }

        for (int i = 0; i < levelInfo.stateLevels.Count; i++)
        {
            if (i == 0)
                levelInfo.stateLevels[i] = StateLevel.Open;
            else
                levelInfo.stateLevels[i] = StateLevel.Close;
        }
    }
}
