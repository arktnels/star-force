﻿using System.Collections.Generic;

[System.Serializable]
public struct LevelInfo
{
    public List<StateLevel> stateLevels;
}
