﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameState : MonoBehaviour, ICustomSignal
{
    private int _health = 3;
    [SerializeField] private Transform _healthImage;
    [SerializeField] private Text _healthText;
    [SerializeField] private Text _score;
    [SerializeField] private Levels _levels;

    private int _maxScore = 10;
    private int _countAsteroids = 0;

    private void Start()
    {
        Translator.AddListener(OnCustomSignal);
        _maxScore = (_levels.GetCurrentLevel() + 1) * 10;
        _score.text = $"Score: {_countAsteroids} / {_maxScore}";
    }
    
    IEnumerator Move(Transform target)
    {
        int count = 0;
        while (count < 100)
        {
            target.Translate(0, 0, 2.0f * Time.deltaTime);
            yield return new WaitForEndOfFrame();
            count++;
        }
        Destroy(target.gameObject);
    }

    public void OnCustomSignal(CodeEvent codeEvent)
    {
        switch (codeEvent)
        {
            case CodeEvent.DeathPlayer:
                _health--;
                _healthText.text = $"x {_health}";
                int count = _healthImage.childCount;
                Transform imageHealth = _healthImage.GetChild(count - 1);
                StartCoroutine(Move(imageHealth));

                if (_health <= 0)
                {
                    Translator.SendSignal(CodeEvent.LoseGame);
                    Translator.SendSignal(CodeEvent.EndGame);
                    Translator.RemoveAll();
                }
                break;
            case CodeEvent.DestroyAsteroid:
                _countAsteroids++;
                _score.text = $"Score: {_countAsteroids} / {_maxScore}";
                if (_countAsteroids == _maxScore)
                {
                    _levels.NextLevel();
                    Translator.SendSignal(CodeEvent.WinGame);
                    Translator.RemoveAll();
                }
                break;
        }
    }
}
