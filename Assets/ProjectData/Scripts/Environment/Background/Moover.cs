﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moover : MonoBehaviour, ICustomSignal
{
    [SerializeField] private Material _mat;
    [Range(0.2f, 3.0f)][SerializeField]
    private float _speed;
    private float _currentSpeed;

    void Start()
    {
        _currentSpeed = _speed;
        Translator.AddListener(OnCustomSignal);
    }

    void Update()
    {
        Vector2 offset = _mat.mainTextureOffset;
        _mat.mainTextureOffset = new Vector2(offset.x, offset.y - _currentSpeed * Time.deltaTime);
    }

    public void OnCustomSignal(CodeEvent codeEvent)
    {
        switch (codeEvent)
        {
            case CodeEvent.StartGame:
                _currentSpeed = _speed;
                break;
            case CodeEvent.PauseGame:
            case CodeEvent.EndGame:
            case CodeEvent.LoseGame:
            case CodeEvent.WinGame:
                _currentSpeed = 0.0f;
                break;
        }
    }
}
