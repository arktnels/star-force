﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct ScreenBorder
{
    public Vector3 min, max;

    public float XMin => min.x;
    public float ZMin => min.z;
    public float XMax => max.x;
    public float ZMax => max.z;

    public ScreenBorder(Vector3 _max, Vector3 _min)
    {
        max = _max;
        min = _min;
    }
}
