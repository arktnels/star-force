﻿using System.Collections.Generic;
using UnityEngine;

public class ScreenController : MonoBehaviour, ICustomSignal
{
    static ScreenBorder border;
    //public Transform target;
    public Vector4 offset; // x - top, y - bottom, z - left, w - right
    private List<Transform> _tracking = new List<Transform>();

    public static ScreenController instance;

    void Start()
    {
        instance = this;

        Translator.AddListener(OnCustomSignal);

        border = new ScreenBorder(Camera.main.ViewportToWorldPoint(Vector3.one),
                                  Camera.main.ViewportToWorldPoint(Vector3.zero));
    }

    void Update()
    {
        foreach (Transform target in _tracking)
            if (IsOverBorder(target))
            {
                Destroy(target.gameObject);
                _tracking.Remove(target);
                break;
            }
    }

    public static bool IsOverBorder(Transform target)
    {
        if (target == null)
            return false;

        bool overTop = target.position.z > border.ZMax;
        bool overBottom = target.position.z < border.ZMin;
        bool overLeft = target.position.x < border.XMin;
        bool overRight = target.position.x > border.XMax;
        bool overScreen = overTop || overBottom || overLeft || overRight;

        if (overScreen)
            return true;
        else
            return false;
    }

    public static void Constraint(Transform target)
    {
        if (target == null)
            return;

        float x = Mathf.Clamp(target.position.x, border.XMin + instance.offset.z, border.XMax - instance.offset.w);
        float z = Mathf.Clamp(target.position.z, border.ZMin + instance.offset.y, border.ZMax - instance.offset.x);

        target.position = new Vector3
        (
            x,
            target.position.y,
            z
        );
    }

    public void AddTracking(Transform target)
    {
        _tracking.Add(target);
    }

    private void ClearTracking()
    {
        foreach (Transform target in _tracking)
            Destroy(target);
    }

    public void OnCustomSignal(CodeEvent codeEvent)
    {
        switch (codeEvent)
        {
            case CodeEvent.RequestScreenSize:
                border = new ScreenBorder(Camera.main.ViewportToWorldPoint(Vector3.one),
                                          Camera.main.ViewportToWorldPoint(Vector3.zero));

                Translator.SendEvent(CodeEvent.AnswerScreenSize, border);
                break;
            case CodeEvent.LoseGame:
            case CodeEvent.WinGame:
            case CodeEvent.EndGame:
                ClearTracking();
                break;
        }
    }
}
