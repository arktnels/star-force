﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    private float _xSpeed, _ySpeed, _zSpeed;
    [SerializeField] private float _speed;
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] ParticleSystem _particle;

    void Start()
    {
        _xSpeed = Random.Range(-5.0f, 5.0f);
        _ySpeed = Random.Range(-5.0f, 5.0f);
        _zSpeed = Random.Range(-5.0f, 5.0f);
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.AddForce(Vector3.forward * -1.0f * _speed, ForceMode.Impulse);
    }

    private void Update()
    {
        transform.Rotate(new Vector3(_xSpeed, _ySpeed, _zSpeed));
    }

    IEnumerator DestroyAsteroid()
    {
        Collider[] colliders = GetComponentsInChildren<Collider>();
        MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();

        foreach (MeshRenderer renderer in renderers)
            renderer.enabled = false;

        foreach (Collider collider in colliders)
            collider.enabled = false;

        _rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        _xSpeed = _ySpeed = _zSpeed = 0.0f;

        yield return new WaitForSeconds(1.0f);
        Destroy(gameObject);
    }

    private bool isDestroy;
    private void OnCollisionEnter(Collision collision)
    {
        if (isDestroy)
            return;

        isDestroy = true;

        switch (collision.gameObject.tag)
        {
            case "Player":
                Translator.SendSignal(CodeEvent.DeathPlayer);
                Destroy(gameObject);
                break;
            case "Bullet":
                SoundControl.instance.PlaySound(Sounds.Crash);
                Translator.SendSignal(CodeEvent.DestroyAsteroid);
                GameObject particle = Instantiate(_particle.gameObject, transform.position, Quaternion.identity);
                particle.transform.localScale = Vector3.one * 0.5f;
                Destroy(collision.gameObject);
                StartCoroutine(DestroyAsteroid());
                Destroy(gameObject);
                break;
        }
    }
}
