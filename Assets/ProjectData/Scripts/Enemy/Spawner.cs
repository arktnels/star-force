﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour, ICustomEvent, ICustomSignal
{
    [SerializeField] private GameObject[] _spawnObjects;
    private float _zPosition;
    private Vector2 _xposition;
    private bool _isSpawn;
    [SerializeField] private float _timeout;
    private float _time;

    private void Start()
    {
        Translator.AddListener(OnCustomEvent);
        Translator.AddListener(OnCustomSignal);
        StartCoroutine(RequestBorder());
    }

    private void Update()
    {
        if (!_isSpawn)
            return;

        if (_time < _timeout)
        {
            _time += Time.deltaTime;
        }
        else
        {
            _time = 0;
            Spawn();
        }
    }

    IEnumerator RequestBorder()
    {
        while (!_isSpawn)
        {
            Translator.SendSignal(CodeEvent.RequestScreenSize);
            yield return new WaitForEndOfFrame();
        }
    }

    private void Spawn()
    {
        int num = Random.Range(0, _spawnObjects.Length);
        GameObject prefab = Instantiate(_spawnObjects[num]);
        prefab.transform.position = new Vector3(Random.Range(_xposition.x, _xposition.y), 1.0f, _zPosition);
        ScreenController.instance.AddTracking(prefab.transform);
    }

    public void OnCustomEvent(CodeEvent codeEvent, object data)
    {
        switch (codeEvent)
        {
            case CodeEvent.AnswerScreenSize:
                ScreenBorder border = (ScreenBorder)data;
                _zPosition = border.ZMax;
                _xposition = new Vector2(border.XMin, border.XMax);
                _isSpawn = true;
                break;
        }
    }

    public void OnCustomSignal(CodeEvent codeEvent)
    {
        switch (codeEvent)
        {
            case CodeEvent.PauseGame:
            case CodeEvent.EndGame:
            case CodeEvent.LoseGame:
            case CodeEvent.WinGame:
                _isSpawn = false;
                break;
        }
    }
}
