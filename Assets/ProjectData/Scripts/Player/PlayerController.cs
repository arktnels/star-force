﻿using UnityEngine;
using System.Collections;


public class PlayerController : MonoBehaviour, ICustomEvent, ICustomSignal
{
    private float _moveHorizontal, _moveVertical;
    public float speed;
    public float speedBullet;
    public float tilt;
    public ScreenBorder border;
    private Rigidbody rigidbody;
    public Transform spawnBullet;
    private Collider[] _colliders;
    private MeshRenderer[] _renderers;
    private Vector3 _startPosition;
    [SerializeField] ParticleSystem _particle;
    private PlayerState _state;

    private void Start()
    {
        Translator.AddListener(OnCustomEvent);
        Translator.AddListener(OnCustomSignal);

        rigidbody = GetComponent<Rigidbody>();

        _colliders = GetComponentsInChildren<Collider>();
        _renderers = GetComponentsInChildren<MeshRenderer>();

        _startPosition = transform.position;
        _state = PlayerState.Live;
    }

    void FixedUpdate()
    {
        Vector3 movement = new Vector3(_moveHorizontal, 0.0f, _moveVertical);
        rigidbody.velocity = movement * speed;

        ScreenController.Constraint(transform);

        GetComponent<Rigidbody>().rotation = Quaternion.Euler(0.0f, 0.0f, GetComponent<Rigidbody>().velocity.x * -tilt);
    }

    private void VisiblePlayer(bool isVisible)
    {
        foreach (MeshRenderer renderer in _renderers)
            renderer.enabled = isVisible;
    }

    private void CollisedPlayer(bool isCollised)
    {
        foreach (Collider collider in _colliders)
            collider.enabled = isCollised;
    }

    IEnumerator Respawn()
    {
        yield return new WaitForSeconds(1.0f);

        transform.position = _startPosition;
        for (int i = 0; i < 7; i++)
        {
            VisiblePlayer(true);
            yield return new WaitForSeconds(0.2f);
            VisiblePlayer(false);
            yield return new WaitForSeconds(0.2f);
        }
        VisiblePlayer(true);
        _state = PlayerState.Live;
        yield return new WaitForSeconds(0.5f);
        CollisedPlayer(true);
    }

    public void OnCustomEvent(CodeEvent codeEvent, object data)
    {
        switch (codeEvent)
        {
            case CodeEvent.InputHorizontal:
                _moveHorizontal = (float)data;
                break;
            case CodeEvent.InputVertical:
                _moveVertical = (float)data;
                break;
        }
    }

    public void OnCustomSignal(CodeEvent codeEvent)
    {
        switch (codeEvent)
        {
            case CodeEvent.InputFire:
                if (_state == PlayerState.Live)
                {
                    SoundControl.instance.PlaySound(Sounds.Shoot);
                    GameObject bullet = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                    bullet.tag = "Bullet";
                    bullet.transform.localScale = Vector3.one * 0.1f;
                    bullet.transform.position = spawnBullet.position;
                    bullet.AddComponent<SphereCollider>();
                    Rigidbody rb = bullet.AddComponent<Rigidbody>();
                    RigidbodyConstraints constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
                    rb.constraints = constraints;
                    rb.useGravity = false;
                    rb.AddForce(Vector3.forward * speedBullet, ForceMode.Impulse);

                    ScreenController.instance.AddTracking(bullet.transform);
                }
                break;
            case CodeEvent.DeathPlayer:
                SoundControl.instance.PlaySound(Sounds.Dead);
                _state = PlayerState.Dead;
                _particle.Play();
                VisiblePlayer(false);
                CollisedPlayer(false);
                StartCoroutine(Respawn());
                break;
            case CodeEvent.WinGame:
            case CodeEvent.LoseGame:
                Translator.RemoveListener(OnCustomSignal);
                Translator.RemoveListener(OnCustomEvent);
                break;
        }
    }
}