﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FireButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private bool _isPush;
    public bool IsPush { get => _isPush; }

    public void OnPointerDown(PointerEventData eventData)
    {
        _isPush = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _isPush = false;
    }
}
