﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputManager : MonoBehaviour
{
    public Joystick joystick;
    public FireButton button;
    private float _axisHr, _axisVr;
    private float _delayFire = 0.2f;
    private float _time = 0;

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKey(KeyCode.Space))
            FireButton(Time.deltaTime);
#else
        if (button.IsPush)
            FireButton(Time.deltaTime);
#endif
    }

    void FixedUpdate()
    {
#if UNITY_EDITOR
        _axisHr = Input.GetAxis("Horizontal");
        _axisVr = Input.GetAxis("Vertical");
#else
        _axisHr = joystick.Horizontal;
        _axisVr = joystick.Vertical;
#endif

        Translator.SendEvent(CodeEvent.InputHorizontal, _axisHr);
        Translator.SendEvent(CodeEvent.InputVertical, _axisVr);
    }

    public void FireButton(float timePassed)
    {
        if (_time < _delayFire)
        {
            _time += timePassed;
        }
        else
        {
            _time = 0;
            Translator.SendSignal(CodeEvent.InputFire);
        }
    }
}
